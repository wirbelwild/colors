<?php

/**
 * Colors.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @license MIT
 */

namespace Color\Tests\Value;

use Color\System\Enum\HKS as HKSEnum;
use Color\System\Exception\InvalidSystem;
use Color\System\HKS as HKSSystem;
use Color\Value\Exception\InvalidInputLengthRangeException;
use Color\Value\Exception\InvalidInputNumberException;
use Color\Value\Exception\InvalidValue;
use Color\Value\HKS;
use Color\Value\ValueInterface;
use PHPUnit\Framework\TestCase;

/**
 * Class HKSTest
 *
 * @package Color\Tests
 */
class HKSTest extends TestCase
{
    /**
     * @throws InvalidInputLengthRangeException
     * @throws InvalidInputNumberException
     * @throws InvalidSystem
     * @throws InvalidValue
     */
    public function testColor(): void
    {
        $hks = new HKS(
            '10',
            new HKSSystem(
                HKSEnum::HKS_K()
            )
        );

        self::assertSame(
            58.0,
            $hks->getCIELab()->getValue('L')
        );
        
        self::assertSame(
            67.0,
            $hks->getCIELab()->getValue('A')
        );
        
        self::assertSame(
            66.0,
            $hks->getCIELab()->getValue('B')
        );
    }

    /**
     * @throws InvalidInputNumberException
     * @throws InvalidSystem
     * @throws InvalidValue
     * @throws InvalidInputLengthRangeException
     */
    public function testCanSerialize(): void
    {
        $hks = new HKS(
            '10',
            new HKSSystem(
                HKSEnum::HKS_K()
            )
        );
        
        $hksSerialized = serialize($hks);
        
        self::assertIsString($hksSerialized);

        self::assertSame(
            382,
            strlen($hksSerialized)
        );

        /** @var ValueInterface $hksUnserialized */
        $hksUnserialized = unserialize($hksSerialized, [ValueInterface::class]);
        
        self::assertInstanceOf(
            HKS::class,
            $hksUnserialized
        );

        self::assertInstanceOf(
            HKSSystem::class,
            $hksUnserialized->getSystem()
        );

        self::assertNotNull(
            $hksUnserialized->getSystem()->getAllColors()
        );
        
        self::assertNotNull(
            $hksUnserialized->getRGB()
        );
    }
}
