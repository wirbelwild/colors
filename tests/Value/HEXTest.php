<?php

/**
 * Colors.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @license MIT
 */

namespace Color\Tests\Value;

use Color\Value\Exception\InvalidInputLengthRangeException;
use Color\Value\Exception\InvalidInputNumberException;
use Color\Value\HEX;
use PHPUnit\Framework\TestCase;

/**
 * Class HEXTest
 *
 * @package Color\Tests\Value
 */
class HEXTest extends TestCase
{
    /**
     * @throws InvalidInputNumberException
     */
    public function testCanHandleShortColorNames(): void
    {
        $hex1 = new HEX('cccccc');
        $hex2 = new HEX('ccc');
        $hex3 = new HEX('#CCC');

        self::assertSame(
            [
                'R' => 204,
                'G' => 204,
                'B' => 204,
            ],
            $hex1->getRGB()->getValues()
        );

        self::assertSame(
            [
                'R' => 204,
                'G' => 204,
                'B' => 204,
            ],
            $hex2->getRGB()->getValues()
        );

        self::assertSame(
            [
                'R' => 204,
                'G' => 204,
                'B' => 204,
            ],
            $hex3->getRGB()->getValues()
        );

        self::assertSame(
            $hex1->getRGB()->getValues(),
            $hex2->getRGB()->getValues()
        );
        
        self::assertSame(
            $hex1->getRGB()->getValues(),
            $hex3->getRGB()->getValues()
        );

        self::assertSame(
            '#cccccc',
            (string) $hex1
        );
        
        self::assertSame(
            '#ccc',
            (string) $hex2
        );
        
        self::assertSame(
            '#CCC',
            (string) $hex3
        );
    }
    
    public function testThrowsExceptionOnInvalidInput(): void
    {
        $this->expectException(InvalidInputLengthRangeException::class);
        $HEX = new HEX('#080C');
        unset($HEX);
    }

    /**
     * @throws InvalidInputNumberException
     */
    public function testCanHandle32bit(): void
    {
        $HEX = new HEX('#008800');
        $RGBA = $HEX->getRGBA();

        self::assertSame(
            [
                'R' => 0,
                'G' => 136,
                'B' => 0,
                'A' => 1.0,
            ],
            $RGBA->getValues()
        );
        
        $HEX = new HEX('#008800CC');
        $RGBA = $HEX->getRGBA();

        self::assertSame(
            [
                'R' => 0,
                'G' => 136,
                'B' => 0,
                'A' => 0.8,
            ],
            $RGBA->getValues()
        );

        self::assertSame(
            '#008800CC',
            (string) $HEX
        );
    }
}
