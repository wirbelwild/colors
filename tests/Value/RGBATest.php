<?php

/**
 * Colors.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @license MIT
 */

namespace Color\Tests\Value;

use Color\Value\Exception\InvalidInputNumberException;
use Color\Value\RGBA;
use PHPUnit\Framework\TestCase;

/**
 * Class RGBATest
 *
 * @package Color\Tests\Value
 */
class RGBATest extends TestCase
{
    /**
     * @throws InvalidInputNumberException
     */
    public function testCanHandleAlpha(): void
    {
        $RGBA = new RGBA(0, 255, 125, .5);

        self::assertSame(0, $RGBA->getValue('R'));
        self::assertSame(255, $RGBA->getValue('G'));
        self::assertSame(125, $RGBA->getValue('B'));
        self::assertSame(.5, $RGBA->getValue('A'));
    }
}
