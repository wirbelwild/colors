<?php

/**
 * Colors.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @license MIT
 */

namespace Color\Tests\Value;

use Color\System\Enum\RAL as RALEnum;
use Color\System\Exception\InvalidSystem;
use Color\System\RAL as RALSystem;
use Color\Value\Exception\InvalidInputLengthRangeException;
use Color\Value\Exception\InvalidInputNumberException;
use Color\Value\Exception\InvalidValue;
use Color\Value\RAL;
use PHPUnit\Framework\TestCase;

/**
 * Class RALTest
 *
 * @package Color\Tests
 */
class RALTest extends TestCase
{
    /**
     * @throws InvalidInputNumberException
     * @throws InvalidSystem
     * @throws InvalidValue
     * @throws InvalidInputLengthRangeException
     */
    public function testColor(): void
    {
        $ral = new RAL(
            '1020',
            new RALSystem(
                RALEnum::RAL()
            )
        );

        self::assertSame(
            0.0,
            $ral->getCMYK()->getValue('C')
        );
        
        self::assertSame(
            15.0,
            $ral->getCMYK()->getValue('M')
        );
        
        self::assertSame(
            76.0,
            $ral->getCMYK()->getValue('Y')
        );
        
        self::assertSame(
            38.0,
            $ral->getCMYK()->getValue('K')
        );
    }
}
