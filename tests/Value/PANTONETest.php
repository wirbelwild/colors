<?php

/**
 * Colors.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @license MIT
 */

namespace Color\Tests\Value;

use Color\System\Enum\PANTONE as PANTONEEnum;
use Color\System\Exception\InvalidSystem;
use Color\System\PANTONE as PANTONESystem;
use Color\Value\Exception\InvalidInputLengthRangeException;
use Color\Value\Exception\InvalidInputNumberException;
use Color\Value\Exception\InvalidValue;
use Color\Value\PANTONE;
use Color\Value\ValueInterface;
use PHPUnit\Framework\TestCase;

/**
 * Class PANTONETest.
 *
 * @package Color\Tests\Value
 */
class PANTONETest extends TestCase
{
    /**
     * @throws InvalidInputNumberException
     * @throws InvalidSystem
     * @throws InvalidValue
     * @throws InvalidInputLengthRangeException
     */
    public function testCanSerialize(): void
    {
        $pantone = new PANTONE(
            'Warm Red',
            new PANTONESystem(
                PANTONEEnum::PANTONE_PLUS_SOLID_COATED()
            )
        );

        $pantoneSerialized = serialize($pantone);

        self::assertIsString($pantoneSerialized);

        self::assertSame(
            423,
            strlen($pantoneSerialized)
        );

        /** @var ValueInterface $pantoneUnserialized */
        $pantoneUnserialized = unserialize($pantoneSerialized, [ValueInterface::class]);

        self::assertInstanceOf(
            PANTONE::class,
            $pantoneUnserialized
        );

        self::assertInstanceOf(
            PANTONESystem::class,
            $pantoneUnserialized->getSystem()
        );

        self::assertNotNull(
            $pantoneUnserialized->getSystem()->getAllColors()
        );

        self::assertNotNull(
            $pantoneUnserialized->getRGB()
        );
    }
}
