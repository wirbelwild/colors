<?php

/**
 * Colors.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @license MIT
 */

namespace Color\Tests\Value;

use Color\System\CMYK as CMYKSystem;
use Color\Value\CMYK;
use Color\Value\Exception\InvalidInputNumberException;
use Color\Value\ValueInterface;
use PHPUnit\Framework\TestCase;

/**
 * Class CMYKTest.
 *
 * @package Color\Tests\Value
 */
class CMYKTest extends TestCase
{
    /**
     * @throws InvalidInputNumberException
     */
    public function testCanThrowException(): void
    {
        $this->expectException(InvalidInputNumberException::class);
        $this->expectExceptionMessage('Input of class "Color\Value\CMYK" needs to be between 0 and 100 but is 101 instead');
        $CMYK = new CMYK(0, 101, 100, 0);
        unset($CMYK);
    }

    /**
     * @throws InvalidInputNumberException
     */
    public function testCanSerialize(): void
    {
        $cmyk = new CMYK(0, 100, 100, 0);

        $cmykSerialized = serialize($cmyk);

        self::assertIsString($cmykSerialized);

        self::assertSame(
            154,
            strlen($cmykSerialized)
        );

        /** @var ValueInterface $cmykUnserialized */
        $cmykUnserialized = unserialize($cmykSerialized, [ValueInterface::class]);

        self::assertInstanceOf(
            CMYK::class,
            $cmykUnserialized
        );

        self::assertInstanceOf(
            CMYKSystem::class,
            $cmykUnserialized->getSystem()
        );

        self::assertNotNull(
            $cmykUnserialized->getSystem()->getAllColors()
        );
    }
}
