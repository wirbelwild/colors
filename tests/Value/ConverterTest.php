<?php

/**
 * Colors.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @license MIT
 */

namespace Color\Tests\Value;

use Color\Value\CIELab;
use Color\Value\CMY;
use Color\Value\CMYK;
use Color\Value\Converter;
use Color\Value\Exception\InvalidInputLengthException;
use Color\Value\Exception\InvalidInputNumberException;
use Color\Value\HEX;
use Color\Value\HSL;
use Color\Value\HSLA;
use Color\Value\RGB;
use Color\Value\RGBA;
use Color\Value\XYZ;
use PHPUnit\Framework\TestCase;

/**
 * Class ConverterTest
 *
 * @package Color\Tests\Value
 */
class ConverterTest extends TestCase
{
    /**
     * @var Converter
     */
    private $converter;

    /**
     * ConverterTest constructor.
     */
    protected function setUp(): void
    {
        $this->converter = new Converter();
    }

    /**
     * Tests if an RGB color can be converted to XYZ
     *
     * @throws InvalidInputNumberException
     */
    public function testGetXYZFromRGB(): void
    {
        $RGB = new RGB(18, 31, 41);
        $XYZ = $this->converter->getXYZFromRGB($RGB);

        self::assertSame(
            [
                'X' => 1.14,
                'Y' => 1.269,
                'Z' => 2.283,
            ],
            $XYZ->getValues()
        );
    }

    /**
     * Tests if a HEX color can be converted to RGB
     *
     * @throws InvalidInputLengthException
     * @throws InvalidInputNumberException
     */
    public function testGetRGBFromHEX(): void
    {
        $HEX = new HEX('121f29');
        $RGB = $this->converter->getRGBFromHEX($HEX);

        self::assertSame(
            [
                'R' => 18,
                'G' => 31,
                'B' => 41,
            ],
            $RGB->getValues()
        );
    }

    /**
     * Tests if a RGB color can be converted to HEX
     *
     * @throws InvalidInputLengthException
     * @throws InvalidInputNumberException
     */
    public function testGetHEXFromRGB(): void
    {
        $RGB = new RGB(18, 31, 41);
        $HEX = $this->converter->getHEXFromRGB($RGB);

        self::assertSame(
            [
                'HEX' => '121f29',
            ],
            $HEX->getValues()
        );
    }

    /**
     * Tests if a CMYK color can be converted to RGB
     *
     * @throws InvalidInputNumberException
     */
    public function testGetRGBFromCMYK(): void
    {
        $CMYK = new CMYK(83, 56, 39, 78);
        $RGB = $this->converter->getRGBFromCMYK($CMYK);

        self::assertSame(
            [
                'R' => 10,
                'G' => 25,
                'B' => 34,
            ],
            $RGB->getValues()
        );
    }

    /**
     * Tests if a XYZ color can be converted to CIELab
     *
     * @throws InvalidInputNumberException
     */
    public function testGetCIELabFromXYZ(): void
    {
        $XYZ = new XYZ(1.13, 1.26, 2.30);
        $CIELab = $this->converter->getCIELabFromXYZ($XYZ);

        self::assertSame(
            [
                'L' => 11.0,
                'A' => -2.0,
                'B' => -9.0,
            ],
            $CIELab->getValues()
        );
    }

    /**
     * Tests if a XYZ color can be converted to RGB
     *
     * @throws InvalidInputNumberException
     */
    public function testGetRGBFromXYZ(): void
    {
        $XYZ = new XYZ(1.13, 1.26, 2.30);
        $RGB = $this->converter->getRGBFromXYZ($XYZ);

        self::assertSame(
            [
                'R' => 17,
                'G' => 31,
                'B' => 41,
            ],
            $RGB->getValues()
        );

        $XYZ = new XYZ(68, 70, 22);
        $RGB = $this->converter->getRGBFromXYZ($XYZ);

        self::assertSame(
            [
                'R' => 255,
                'G' => 213,
                'B' => 100,
            ],
            $RGB->getValues()
        );
        
        $XYZ = new XYZ(17, 19, 59);
        $RGB = $this->converter->getRGBFromXYZ($XYZ);

        self::assertSame(
            [
                'R' => 0,
                'G' => 128,
                'B' => 203,
            ],
            $RGB->getValues()
        );
    }

    /**
     * Tests if a CMYK color can be converted to CMY
     *
     * @throws InvalidInputNumberException
     */
    public function testGetCMYFromCMYK(): void
    {
        $CMYK = new CMYK(83, 56, 39, 78);
        $CMY = $this->converter->getCMYFromCMYK($CMYK);
        
        self::assertSame(
            [
                'C' => 96.0,
                'M' => 90.0,
                'Y' => 87.0,
            ],
            $CMY->getValues()
        );
    }

    /**
     * Tests if a RGB color can be converted to CMYK
     *
     * @throws InvalidInputNumberException
     */
    public function testGetCMYFromRGB(): void
    {
        $RGB = new RGB(18, 31, 41);
        $CMY = $this->converter->getCMYFromRGB($RGB);

        self::assertSame(
            [
                'C' => 93.0,
                'M' => 88.0,
                'Y' => 84.0,
            ],
            $CMY->getValues()
        );
    }

    /**
     * Tests if a CIELab color can be converted to XYZ
     *
     * @throws InvalidInputNumberException
     */
    public function testGetXYZFromCIELab(): void
    {
        $CIELab = new CIELab(11, -2, -9);
        $XYZ = $this->converter->getXYZFromCIELab($CIELab);

        self::assertSame(
            [
                'X' => 1.135,
                'Y' => 1.261,
                'Z' => 2.299,
            ],
            $XYZ->getValues()
        );
    }

    /**
     * Tests if a CMY color can be converted to RGB
     *
     * @throws InvalidInputNumberException
     */
    public function testGetRGBFromCMY(): void
    {
        $CMY = new CMY(93, 88, 84);
        $RGB = $this->converter->getRGBFromCMY($CMY);
        
        self::assertSame(
            [
                'R' => 17,
                'G' => 30,
                'B' => 40,
            ],
            $RGB->getValues()
        );
    }

    /**
     * Tests if a CMY color can be converted to CMYK
     *
     * @throws InvalidInputNumberException
     */
    public function testGetCMYKFromCMY(): void
    {
        $CMY = new CMY(93, 88, 84);
        $CMYK = $this->converter->getCMYKFromCMY($CMY);

        self::assertSame(
            [
                'C' => 56.0,
                'M' => 25.0,
                'Y' => 0.0,
                'K' => 84.0,
            ],
            $CMYK->getValues()
        );
    }

    /**
     * Tests if a RGB color can be converted to RGBA
     *
     * @throws InvalidInputNumberException
     */
    public function testGetRGBAFromRGB(): void
    {
        $RGB = new RGB(18, 31, 41);
        $RGBA = $this->converter->getRGBAFromRGB($RGB);
        
        self::assertSame(
            [
                'R' => 18,
                'G' => 31,
                'B' => 41,
                'A' => 1.0,
            ],
            $RGBA->getValues()
        );
    }

    /**
     * Tests if a RGBA color can be converted to RGB
     *
     * @throws InvalidInputNumberException
     */
    public function testGetRGBFromRGBA(): void
    {
        $RGBA = new RGBA(18, 31, 41, .5);
        $RGB = $this->converter->getRGBFromRGBA($RGBA);

        self::assertSame(
            [
                'R' => 9,
                'G' => 15,
                'B' => 20,
            ],
            $RGB->getValues()
        );
    }

    /**
     * Tests if a RGB color can be converted to HSL
     *
     * @throws InvalidInputNumberException
     */
    public function testGetHSLFromRGB(): void
    {
        $RGB = new RGB(18, 31, 41);
        $HSL = $this->converter->getHSLFromRGB($RGB);

        self::assertSame(
            [
                'H' => 206,
                'S' => 39,
                'L' => 12,
            ],
            $HSL->getValues()
        );
    }

    /**
     * Tests if a HSL color can be converted to RGB
     *
     * @throws InvalidInputNumberException
     */
    public function testGetRGBFromHSL(): void
    {
        $HSL = new HSL(206, 39, 12);
        $RGB = $this->converter->getRGBFromHSL($HSL);

        self::assertSame(
            [
                'R' => 19,
                'G' => 32,
                'B' => 43,
            ],
            $RGB->getValues()
        );
    }

    /**
     * Tests if a HSL color can be converted to HSLA
     *
     * @throws InvalidInputNumberException
     */
    public function testGetHSLAFromHSL(): void
    {
        $HSL = new HSL(206, 39, 12);
        $HSLA = $this->converter->getHSLAFromHSL($HSL);

        self::assertSame(
            [
                'H' => 206,
                'S' => 39,
                'L' => 12,
                'A' => 1.0,
            ],
            $HSLA->getValues()
        );
    }

    /**
     * Tests if a HSLA color can be converted to RGB
     *
     * @throws InvalidInputNumberException
     */
    public function testGetRGBAFromHSLA(): void
    {
        $HSLA = new HSLA(206, 39, 12, .5);
        $RGBA = $this->converter->getRGBAFromHSLA($HSLA);

        self::assertSame(
            [
                'R' => 19,
                'G' => 32,
                'B' => 43,
                'A' => 0.5,
            ],
            $RGBA->getValues()
        );
    }
}
