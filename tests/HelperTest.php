<?php

/**
 * Colors.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @license MIT
 */

namespace Color\Tests;

use Color\Helper;
use Generator;
use PHPUnit\Framework\TestCase;

/**
 * Class HelperTest.
 *
 * @package Color\Tests
 */
class HelperTest extends TestCase
{
    /**
     * @dataProvider getValues
     * @param float $percentage
     * @param string $hexValue
     */
    public function testCanConvertPercentToHex(float $percentage, string $hexValue): void
    {
        self::assertSame(
            $hexValue,
            Helper::getHexFromPercent($percentage)
        );
    }

    /**
     * @dataProvider getValues
     * @param float $percentage
     * @param string $hexValue
     */
    public function testCanConvertHexToPercent(float $percentage, string $hexValue): void
    {
        self::assertSame(
            $percentage,
            Helper::getPercentFromHex($hexValue)
        );
    }

    /**
     * @return Generator<array<int, (float | string)>>
     */
    public function getValues(): Generator
    {
        yield [0, '00'];
        yield [50, '80'];
        yield [80, 'cc'];
        yield [100, 'ff'];
    }
}
