<?php

/**
 * Colors.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @license MIT
 */

namespace Color\Tests;

use Color\Value\CMYK;
use Color\Value\Exception\InvalidInputLengthException;
use Color\Value\Exception\InvalidInputNumberException;
use PHPUnit\Framework\TestCase;

/**
 * Class CompareColorsTest
 *
 * @package Color\Tests
 */
class CompareColorsTest extends TestCase
{
    /**
     * Tests if a the conversion from CMYK to RGB to CMYK returns same values
     *
     * @throws InvalidInputNumberException
     */
    public function testCMYKtoRGBtoCMYK(): void
    {
        $color = new CMYK(0, 100, 90, 0);
        
        $original = [
            $color->getValue('C'),
            $color->getValue('M'),
            $color->getValue('Y'),
            $color->getValue('K'),
        ];
        
        $colorConverted = $color->getRGB()->getCMYK();

        $converted = [
            $colorConverted->getValue('C'),
            $colorConverted->getValue('M'),
            $colorConverted->getValue('Y'),
            $colorConverted->getValue('K'),
        ];

        self::assertSame(
            $original,
            $converted
        );
    }

    /**
     * Tests if a the conversion from CMYK to HEX to CMYK returns same values
     *
     * @throws InvalidInputLengthException
     * @throws InvalidInputNumberException
     */
    public function testCMYKtoHEXtoCMYK(): void
    {
        $color = new CMYK(0, 100, 90, 0);
        
        $original = [
            $color->getValue('C'),
            $color->getValue('M'),
            $color->getValue('Y'),
            $color->getValue('K'),
        ];
        
        $colorConverted = $color->getHEX()->getCMYK();

        $converted = [
            $colorConverted->getValue('C'),
            $colorConverted->getValue('M'),
            $colorConverted->getValue('Y'),
            $colorConverted->getValue('K'),
        ];

        self::assertSame(
            $original,
            $converted
        );
    }
}
