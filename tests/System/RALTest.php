<?php

/**
 * Colors.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @license MIT
 */

namespace Color\Tests\System;

use Color\System\Enum\RAL as RALEnum;
use Color\System\Exception\InvalidSystem;
use Color\System\RAL;
use Color\Value\Exception\InvalidInputLengthException;
use Color\Value\Exception\InvalidInputNumberException;
use Color\Value\Exception\InvalidValue;
use PHPUnit\Framework\TestCase;

/**
 * Class RALTest
 *
 * @package Color\Tests\System
 */
class RALTest extends TestCase
{
    /**
     * @throws InvalidSystem
     * @throws InvalidInputLengthException
     * @throws InvalidInputNumberException
     * @throws InvalidValue
     */
    public function testHandlesMissingColor1(): void
    {
        $this->expectException(InvalidValue::class);

        $ralSystem = new RAL(
            RALEnum::RAL()
        );

        $ralSystem->getColor('MISSING_COLOR');
    }

    /**
     * @throws InvalidSystem
     * @throws InvalidInputLengthException
     * @throws InvalidInputNumberException
     * @throws InvalidValue
     */
    public function testHandlesMissingColor2(): void
    {
        $this->expectException(InvalidValue::class);

        $ralSystem = new RAL(
            RALEnum::RAL()
        );

        $ralSystem->getColorInformation('MISSING_COLOR');
    }
}
