# Changes in Bit&Black Colors 2.11

## 2.11.2 2021-10-07

## Fixed

- Fixed wrong Docblocks.
- Fixed wrong composer.json command.

## 2.11.1 2021-04-29

### Fixed

- Fixed missing `Converter` instance after unserialization. 

## 2.11.0 2021-04-29

### Added

- All classes implement the methods `__serialize` and `__unserialize` now. 
  They handle a tiny representation of an object, so it's possible to store them without having a huge bunge of data.