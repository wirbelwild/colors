<?php

/**
 * Colors.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @license MIT
 */

namespace Color\System;

use Color\ColorValueTrait;
use Color\System\Enum\RAL as RALEnum;
use Color\System\Exception\InvalidSystem;
use Color\Value\Collection;
use Color\Value\Exception\InvalidInputLengthRangeException;
use Color\Value\Exception\InvalidInputNumberException;
use Color\Value\Exception\InvalidValue;
use Color\Value\RAL as RALColor;
use Color\Value\ValueInterface;

/**
 * Class RAL
 * @package Color\SystemInterface
 * @see \Color\Tests\System\RALTest
 */
class RAL implements SystemInterface
{
    use ColorValueTrait;
    
    /**
     * All color data
     *
     * @var array<string, array<string, mixed>>
     */
    private $colors;

    /**
     * @var RALEnum
     */
    private $colorSystem;

    /**
     * @var string
     */
    private $colorSystemPrefix;

    /**
     * @var string
     */
    private $colorSystemSuffix;
    
    /**
     * @var \Color\System\Enum\RAL
     */
    private $colorSystemEnum;

    /**
     * Sets up the RAL system
     *
     * @param \Color\System\Enum\RAL $colorSystem
     * @throws InvalidSystem
     * @throws InvalidInputLengthRangeException
     * @throws InvalidInputNumberException
     */
    public function __construct(RALEnum $colorSystem)
    {
        $this->colorSystemEnum = $colorSystem;
        $this->loadInformation($this->colorSystemEnum);
        $this->colors = $this->prepareColors($this->colors);
    }

    /**
     * @return array<string, mixed>
     */
    public function __serialize(): array
    {
        return [
            'cSEC' => get_class($this->colorSystemEnum),
            'cSEK' => $this->colorSystemEnum->getKey(),
        ];
    }

    /**
     * @param array<string, mixed> $data
     * @throws InvalidSystem
     */
    public function __unserialize(array $data): void
    {
        $enum = $data['cSEC'];
        $enumProperty = $data['cSEK'];
        $this->colorSystemEnum = $enum::$enumProperty();
        $this->loadInformation($this->colorSystemEnum);
    }

    /**
     * @param \Color\System\Enum\RAL $colorSystem
     * @throws InvalidSystem
     */
    private function loadInformation(RALEnum $colorSystem): void
    {
        $colorSystemFileName = strtolower($colorSystem);
        $colorSystemFileName = str_replace(' ', '-', $colorSystemFileName);

        $colorFile = dirname(__FILE__, 3) . DIRECTORY_SEPARATOR .
            'data' . DIRECTORY_SEPARATOR .
            $colorSystemFileName . '.json'
        ;

        if (!file_exists($colorFile)) {
            throw new InvalidSystem($colorSystem);
        }

        $colorFile = (string) file_get_contents($colorFile);
        $file = json_decode($colorFile, true);

        $this->colors = $file['values'];
        $this->colorSystem = $file['name']['system'];
        $this->colorSystemPrefix = $file['name']['prefix'];
        $this->colorSystemSuffix = $file['name']['suffix'];
    }

    /**
     * Returns a color
     *
     * @param string $colorName
     * @return ValueInterface
     * @throws InvalidValue
     */
    public function getColor(string $colorName): ValueInterface
    {
        $color = $this->getColorInformation($colorName);
        return new RALColor($color['name'], $this);
    }

    /**
     * Returns all color information
     *
     * @param string $colorName
     * @return array<string, string>
     * @throws InvalidValue
     */
    public function getColorInformation(string $colorName): array
    {
        foreach ($this->colors as $color) {
            if ($colorName === $color['name']) {
                return $color;
            }
        }
        
        throw new InvalidValue($colorName);
    }

    /**
     * Returns a collection of all colors
     *
     * @return Collection<ValueInterface>
     * @throws InvalidValue
     */
    public function getAllColors(): Collection
    {
        $colors = new Collection();
                
        foreach ($this->colors as $color) {
            $RAL = new RALColor($color['name'], $this);
            $colors->add($RAL);
        }
        
        return $colors;
    }

    /**
     * Returns a collection of matching colors
     *
     * @param ValueInterface $color
     * @param int $tolerance
     * @return Collection<ValueInterface>
     * @throws InvalidValue
     */
    public function findColor(ValueInterface $color, int $tolerance = 0): Collection
    {
        $collection = new Collection();
        $originColor = $color->getCIELab();

        foreach ($this->colors as $colorExisting) {
            /** @var ValueInterface $colorValue */
            $colorValue = array_values($colorExisting['values'])[0];
            $colorValue = $colorValue->getCIELab();

            $lMin = $colorValue->getValue('L') - $tolerance;
            $lMax = $colorValue->getValue('L') + $tolerance;
            
            $aMin = $colorValue->getValue('A') - $tolerance;
            $aMax = $colorValue->getValue('A') + $tolerance;

            $bMin = $colorValue->getValue('B') - $tolerance;
            $bMax = $colorValue->getValue('B') + $tolerance;

            $L = $originColor->getValue('L');
            $A = $originColor->getValue('A');
            $B = $originColor->getValue('B');

            if ($lMax >= $L && $lMin <= $L
                && $aMax >= $A && $aMin <= $A
                && $bMax >= $B && $bMin <= $B
            ) {
                $RAL = new RALColor($colorExisting['name'], $this);
                $collection->add($RAL);
            }
        }
        
        return $collection;
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return $this->getColorSystem();
    }

    /**
     * @return string
     */
    public function getColorSystem(): string
    {
        return $this->colorSystem;
    }

    /**
     * Returns the prefix of a color name
     *
     * @return string
     */
    public function getColorSystemPrefix(): string
    {
        return $this->colorSystemPrefix;
    }

    /**
     * Returns the suffix of a color name
     *
     * @return string
     */
    public function getColorSystemSuffix(): string
    {
        return $this->colorSystemSuffix;
    }
}
