<?php

/**
 * Colors.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @license MIT
 */

namespace Color\System;

use Color\Value\Collection;
use Color\Value\NullColor;
use Color\Value\ValueInterface;

/**
 * Class RGB
 * @package Color\System
 */
class RGB implements SystemInterface
{
    /**
     * @var string
     */
    private $colorSystem;

    /**
     * RGB constructor.
     */
    public function __construct()
    {
        $this->colorSystem = 'RGB';
    }

    /**
     * @return array<string, mixed>
     */
    public function __serialize(): array
    {
        return [
            'cS' => $this->colorSystem,
        ];
    }

    /**
     * @param array<string, mixed> $data
     */
    public function __unserialize(array $data): void
    {
        $this->colorSystem = $data['cS'];
    }

    /**
     * @param ValueInterface $color
     * @param int $tolerance
     * @return Collection<ValueInterface>
     */
    public function findColor(ValueInterface $color, int $tolerance = 0): Collection
    {
        return new Collection();
    }

    /**
     * @param string $colorName
     * @return ValueInterface
     */
    public function getColor(string $colorName): ValueInterface
    {
        return new NullColor($colorName);
    }

    /**
     * @return Collection<ValueInterface>
     */
    public function getAllColors(): Collection
    {
        return new Collection();
    }

    /**
     * @param string $colorName
     * @return array<string, string>
     */
    public function getColorInformation(string $colorName): array
    {
        return [
            'name' => $colorName,
        ];
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return $this->getColorSystem();
    }

    /**
     * @return string
     */
    public function getColorSystem(): string
    {
        return $this->colorSystem;
    }

    /**
     * Returns the prefix of a color name
     *
     * @return string
     */
    public function getColorSystemPrefix(): string
    {
        return '';
    }

    /**
     * Returns the suffix of a color name
     *
     * @return string
     */
    public function getColorSystemSuffix(): string
    {
        return '';
    }
}
