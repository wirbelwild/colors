<?php

/**
 * Colors.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @license MIT
 */

namespace Color\Value;

use Color\System\HSL as HSLSystem;
use Color\System\SystemInterface;
use Color\Value\Exception\InvalidInputLengthRangeException;
use Color\Value\Exception\InvalidInputNumberException;

/**
 * Class HSL
 *
 * @package Color\Value
 */
class HSL implements ValueInterface
{
    /**
     * @var array<string, int>
     */
    private $colorInformation;
    
    /**
     * @var \Color\System\HSL
     */
    private $colorSystem;
    
    /**
     * @var Converter
     */
    private $converter;
    
    /**
     * @var string
     */
    private $name;

    /**
     * HSL constructor.
     *
     * @param int $H
     * @param int $S
     * @param int $L
     * @param \Color\System\HSL|null $colorSystem
     * @throws InvalidInputNumberException
     */
    public function __construct(int $H, int $S, int $L, ?HSLSystem $colorSystem = null)
    {
        ValueValidation::validateNumberRange($H, 0, 360);
        ValueValidation::validateNumberRange($S, 0, 100);
        ValueValidation::validateNumberRange($L, 0, 100);

        $this->colorInformation = [
            'H' => $H,
            'S' => $S,
            'L' => $L,
        ];
        $this->colorSystem = $colorSystem ?? new HSLSystem();
        $this->converter = new Converter();
        $this->name = implode(
            ';',
            array_map(
                static function ($key, $value) {
                    return $key . '=' . $value;
                },
                array_keys($this->colorInformation),
                array_values($this->colorInformation)
            )
        );
    }

    /**
     * @return array<string, mixed>
     */
    public function __serialize(): array
    {
        return [
            'cI' => $this->colorInformation,
            'cSC' => $this->colorSystem,
        ];
    }

    /**
     * @param array<string, mixed> $data
     */
    public function __unserialize(array $data): void
    {
        $this->colorInformation = $data['cI'];
        $this->colorSystem = $data['cSC'];
        $this->converter = new Converter();
    }

    /**
     * Allows to simply echo the color to receive its values
     *
     * @return string
     */
    public function __toString(): string
    {
        return implode(', ', $this->colorInformation);
    }

    /**
     * Returns the colors name
     *
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * Returns a single color value
     *
     * @param string $value
     * @return int
     */
    public function getValue(string $value): int
    {
        return $this->colorInformation[$value];
    }

    /**
     * Returns all color values
     *
     * @return array<string, int>
     */
    public function getValues(): array
    {
        return $this->colorInformation;
    }

    /**
     * Returns the color value as a formatted string
     *
     * @param string $format
     * @return string
     */
    public function getFormattedValue(string $format): string
    {
        return sprintf($format, ...array_values($this->colorInformation));
    }

    /**
     * Returns the color system
     *
     * @return SystemInterface
     */
    public function getSystem(): SystemInterface
    {
        return $this->colorSystem;
    }

    /**
     * Returns color values in RGB
     *
     * @return RGB
     * @throws InvalidInputNumberException
     */
    public function getRGB(): RGB
    {
        $HSL = $this->getHSL();
        return $this->converter->getRGBFromHSL($HSL);
    }

    /**
     * Returns color values in RGBA
     *
     * @return RGBA
     * @throws InvalidInputNumberException
     */
    public function getRGBA(): RGBA
    {
        $HSL = $this->getHSL();
        $RGB = $this->converter->getRGBFromHSL($HSL);
        return $this->converter->getRGBAFromRGB($RGB);
    }

    /**
     * Returns color values in CMYK
     *
     * @return CMYK
     * @throws InvalidInputNumberException
     */
    public function getCMYK(): CMYK
    {
        $HSL = $this->getHSL();
        $RGB = $this->converter->getRGBFromHSL($HSL);
        $CMY = $this->converter->getCMYFromRGB($RGB);
        return $this->converter->getCMYKFromCMY($CMY);
    }

    /**
     * Returns color values in HEX
     *
     * @return HEX
     * @throws InvalidInputLengthRangeException
     * @throws InvalidInputNumberException
     */
    public function getHEX(): HEX
    {
        $HSL = $this->getHSL();
        $RGB = $this->converter->getRGBFromHSL($HSL);
        return $this->converter->getHEXFromRGB($RGB);
    }

    /**
     * Returns color values in CIELab
     *
     * @return CIELab
     * @throws InvalidInputNumberException
     */
    public function getCIELab(): CIELab
    {
        $HSL = $this->getHSL();
        $RGB = $this->converter->getRGBFromHSL($HSL);
        $XYZ = $this->converter->getXYZFromRGB($RGB);
        return $this->converter->getCIELabFromXYZ($XYZ);
    }

    /**
     * Returns color values in HSL
     *
     * @return \Color\Value\HSL
     */
    public function getHSL(): HSL
    {
        return $this;
    }

    /**
     * Returns color values in HSLA
     *
     * @return HSLA
     * @throws InvalidInputNumberException
     */
    public function getHSLA(): HSLA
    {
        $HSL = $this->getHSL();
        return $this->converter->getHSLAFromHSL($HSL);
    }

    /**
     * Returns the complementary color
     *
     * @return Complementary
     * @throws InvalidInputNumberException
     */
    public function getComplementary(): Complementary
    {
        return new Complementary($this->getHSL());
    }
}
