<?php

/**
 * Colors.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @license MIT
 */

namespace Color\Value;

use Color\Value\Exception\InvalidInputNumberException;

/**
 * Class Complementary.
 * This class returns the complementary color.
 *
 * @package Color\Value
 * @see \Color\Tests\Value\ComplementaryTest
 */
class Complementary extends HSL
{
    /**
     * Complementary constructor.
     *
     * @param HSL $HSL
     * @throws InvalidInputNumberException
     */
    public function __construct(HSL $HSL)
    {
        $H = $HSL->getValue('H');
        $H += $H > 180 ? -180 : 180;
        
        parent::__construct(
            $H,
            $HSL->getValue('S'),
            $HSL->getValue('L')
        );
    }

    /**
     * Returns the complementary color
     *
     * @return \Color\Value\Complementary
     */
    public function getComplementary(): Complementary
    {
        return $this;
    }
}
