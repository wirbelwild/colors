<?php

/**
 * Colors.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @license MIT
 */

namespace Color\Value;

use Color\Value\Exception\InvalidInputLengthException;
use Color\Value\Exception\InvalidInputLengthRangeException;
use Color\Value\Exception\InvalidInputNumberException;

/**
 * Class ValueValidation
 *
 * @package Color\Value
 */
class ValueValidation
{
    /**
     * @param int|float $input
     * @param int $min
     * @param int $max
     * @throws InvalidInputNumberException
     * @return void
     */
    public static function validateNumberRange($input, int $min, int $max): void
    {
        if ($input < $min || $input > $max) {
            throw new InvalidInputNumberException($input, $min, $max, self::getCallingClass());
        }
    }

    /**
     * @param string|int|float $input
     * @param int $min
     * @param int $max
     * @throws InvalidInputLengthException
     * @return void
     */
    public static function validateLengthRange($input, int $min, int $max): void
    {
        $length = strlen((string) $input);

        if ($length < $min || $length > $max) {
            throw new InvalidInputLengthException($input, $min, $max, self::getCallingClass());
        }
    }

    /**
     * @param string|int|float $input
     * @param int ...$allowedValues
     * @return void
     * @throws InvalidInputLengthRangeException
     */
    public static function validateLengthOptional($input, int ...$allowedValues): void
    {
        $length = strlen((string) $input);
        
        if (!in_array($length, $allowedValues)) {
            throw new InvalidInputLengthRangeException($length, $allowedValues, self::getCallingClass());
        }
    }

    /**
     * @return string
     */
    private static function getCallingClass(): string
    {
        $trace = debug_backtrace();
        $class = $trace[1]['class'];
        $traceCount = count($trace);
        
        for ($counter = 1; $counter < $traceCount; ++$counter) {
            if (isset($trace[$counter]) && $class !== $trace[$counter]['class']) {
                return $trace[$counter]['class'];
            }
        }
        
        return '';
    }
}
