<?php

/**
 * Colors.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @license MIT
 */

namespace Color\Value;

use Color\System\RGB as RGBSystem;
use Color\System\SystemInterface;
use Color\Value\Exception\InvalidInputLengthRangeException;
use Color\Value\Exception\InvalidInputNumberException;

/**
 * Holds RGB values
 */
class RGB implements ValueInterface
{
    /**
     * Instance of converter
     *
     * @var Converter
     */
    private $converter;
    
    /**
     * Color values
     *
     * @var array<string, int>
     */
    private $colorInformation;
    
    /**
     * @var \Color\System\RGB
     */
    private $colorSystem;
    
    /**
     * @var string
     */
    private $name;

    /**
     * Set up a color
     *
     * @param int $R The red value
     * @param int $G The green value
     * @param int $B The blue value
     * @param \Color\System\RGB|null $colorSystem
     * @throws InvalidInputNumberException
     */
    public function __construct(int $R, int $G, int $B, ?RGBSystem $colorSystem = null)
    {
        ValueValidation::validateNumberRange($R, 0, 255);
        ValueValidation::validateNumberRange($G, 0, 255);
        ValueValidation::validateNumberRange($B, 0, 255);
        
        $this->colorInformation = [
            'R' => $R,
            'G' => $G,
            'B' => $B,
        ];
        
        $this->colorSystem = $colorSystem ?? new RGBSystem();
        $this->converter = new Converter();
        $this->name = implode(
            ';',
            array_map(
                static function ($key, $value) {
                    return $key . '=' . $value;
                },
                array_keys($this->colorInformation),
                array_values($this->colorInformation)
            )
        );
    }
    
    /**
     * @return array<string, mixed>
     */
    public function __serialize(): array
    {
        return [
            'cI' => $this->colorInformation,
            'cSC' => $this->colorSystem,
        ];
    }

    /**
     * @param array<string, mixed> $data
     */
    public function __unserialize(array $data): void
    {
        $this->colorInformation = $data['cI'];
        $this->colorSystem = $data['cSC'];
        $this->converter = new Converter();
    }

    /**
     * Returns the color system
     *
     * @return SystemInterface
     */
    public function getSystem(): SystemInterface
    {
        return $this->colorSystem;
    }
    
    /**
     * Returns a single color value
     *
     * @param string $value
     * @return int
     */
    public function getValue(string $value): int
    {
        return $this->colorInformation[$value];
    }

    /**
     * Returns color values in RGB
     *
     * @return RGB
     */
    public function getRGB(): RGB
    {
        return $this;
    }

    /**
     * Returns color values in RGBA
     *
     * @return RGBA
     * @throws InvalidInputNumberException
     */
    public function getRGBA(): RGBA
    {
        return new RGBA(
            $this->getValue('R'),
            $this->getValue('G'),
            $this->getValue('B'),
            1
        );
    }

    /**
     * Returns color values in CMYK
     *
     * @return CMYK
     * @throws InvalidInputNumberException
     */
    public function getCMYK(): CMYK
    {
        $RGB = $this->getRGB();
        $CMY = $this->converter->getCMYFromRGB($RGB);
        return $this->converter->getCMYKFromCMY($CMY);
    }

    /**
     * Returns color values in HEX
     *
     * @return HEX
     * @throws InvalidInputLengthRangeException
     */
    public function getHEX(): HEX
    {
        $RGB = $this->getRGB();
        return $this->converter->getHEXFromRGB($RGB);
    }

    /**
     * Returns color values in CIELab
     *
     * @return CIELab
     * @throws InvalidInputNumberException
     */
    public function getCIELab(): CIELab
    {
        $RGB = $this->getRGB();
        $XYZ = $this->converter->getXYZFromRGB($RGB);
        return $this->converter->getCIELabFromXYZ($XYZ);
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return implode(', ', $this->colorInformation);
    }

    /**
     * Returns the color value as a formatted string
     *
     * @param string $format
     * @return string
     */
    public function getFormattedValue(string $format): string
    {
        return sprintf($format, ...array_values($this->colorInformation));
    }

    /**
     * Returns all color values
     *
     * @return array<string, int>
     */
    public function getValues(): array
    {
        return $this->colorInformation;
    }

    /**
     * Returns color values in HSL
     *
     * @return HSL
     * @throws InvalidInputNumberException
     */
    public function getHSL(): HSL
    {
        $RGB = $this->getRGB();
        return $this->converter->getHSLFromRGB($RGB);
    }

    /**
     * Returns color values in HSLA
     *
     * @return HSLA
     * @throws InvalidInputNumberException
     */
    public function getHSLA(): HSLA
    {
        $HSL = $this->getHSL();
        return $this->converter->getHSLAFromHSL($HSL);
    }

    /**
     * Returns the complementary color
     *
     * @return Complementary
     * @throws InvalidInputNumberException
     */
    public function getComplementary(): Complementary
    {
        return new Complementary($this->getHSL());
    }
}
