<?php

/**
 * Colors.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @license MIT
 */

namespace Color\Value\Exception;

use BitAndBlack\SentenceConstruction;
use Color\Exception;

/**
 * Class InvalidInputLengthRangeException
 *
 * @package Color\Value\Exception
 */
class InvalidInputLengthRangeException extends Exception
{
    /**
     * InvalidInputLengthRangeException constructor.
     *
     * @param int $currentLength
     * @param array<int, int> $allowedValues
     * @param string $class
     */
    public function __construct(int $currentLength, array $allowedValues, string $class)
    {
        parent::__construct(
            'Input of class "' . $class . '" needs to be have a length of ' .
            new SentenceConstruction(', ', ' or ', $allowedValues) .
            ' characters, but has ' . $currentLength . ' instead.'
        );
    }
}
