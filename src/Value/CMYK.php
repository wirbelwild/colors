<?php

/**
 * Colors.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @license MIT
 */

namespace Color\Value;

use Color\System\CMYK as CMYKSystem;
use Color\System\SystemInterface;
use Color\Value\Exception\InvalidInputLengthRangeException;
use Color\Value\Exception\InvalidInputNumberException;

/**
 * Holds CMYK values
 * @see \Color\Tests\Value\CMYKTest
 */
class CMYK implements ValueInterface
{
    /**
     * Instance of converter
     *
     * @var Converter
     */
    private $converter;
    
    /**
     * Color values
     *
     * @var array<string, float>
     */
    private $colorInformation;
    
    /**
     * @var \Color\System\CMYK
     */
    private $colorSystem;
    
    /**
     * @var string
     */
    private $name;

    /**
     * Set up a color
     *
     * @param float $C The Cyan value
     * @param float $M The Magenta value
     * @param float $Y The Yellow value
     * @param float $K The Black value
     * @param \Color\System\CMYK|null $colorSystem
     * @throws InvalidInputNumberException
     */
    public function __construct(float $C, float $M, float $Y, float $K, ?CMYKSystem $colorSystem = null)
    {
        ValueValidation::validateNumberRange($C, 0, 100);
        ValueValidation::validateNumberRange($M, 0, 100);
        ValueValidation::validateNumberRange($Y, 0, 100);
        ValueValidation::validateNumberRange($K, 0, 100);

        $this->colorInformation = [
            'C' => $C,
            'M' => $M,
            'Y' => $Y,
            'K' => $K,
        ];
        $this->colorSystem = $colorSystem ?? new CMYKSystem();
        $this->converter = new Converter();
        $this->name = implode(
            ';',
            array_map(
                static function ($key, $value) {
                    return $key . '=' . $value;
                },
                array_keys($this->colorInformation),
                array_values($this->colorInformation)
            )
        );
    }


    /**
     * @return array<string, mixed>
     */
    public function __serialize(): array
    {
        return [
            'cI' => $this->colorInformation,
            'cSC' => $this->colorSystem,
        ];
    }

    /**
     * @param array<string, mixed> $data
     */
    public function __unserialize(array $data): void
    {
        $this->colorInformation = $data['cI'];
        $this->colorSystem = $data['cSC'];
        $this->converter = new Converter();
    }

    /**
     * Returns the color system
     *
     * @return SystemInterface
     */
    public function getSystem(): SystemInterface
    {
        return $this->colorSystem;
    }
    
    /**
     * Returns a single color value
     *
     * @param string $value
     * @return float
     */
    public function getValue(string $value): float
    {
        return $this->colorInformation[$value];
    }

    /**
     * Returns color values in RGB
     *
     * @return RGB
     * @throws InvalidInputNumberException
     */
    public function getRGB(): RGB
    {
        $CMYK = $this->getCMYK();
        return $this->converter->getRGBFromCMYK($CMYK);
    }

    /**
     * Returns color values in CIELab
     *
     * @return CIELab
     * @throws InvalidInputNumberException
     */
    public function getCIELab(): CIELab
    {
        $RGB = $this->getRGB();
        $XYZ = $this->converter->getXYZFromRGB($RGB);
        return $this->converter->getCIELabFromXYZ($XYZ);
    }

    /**
     * Returns color values in HEX
     *
     * @return HEX
     * @throws InvalidInputLengthRangeException
     * @throws InvalidInputNumberException
     */
    public function getHEX(): HEX
    {
        $RGB = $this->getRGB();
        return $this->converter->getHEXFromRGB($RGB);
    }
    
    /**
     * Returns color values in CMYK
     *
     * @return CMYK
     */
    public function getCMYK(): CMYK
    {
        return $this;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return implode('/', $this->colorInformation);
    }

    /**
     * Returns the color value as a formatted string
     *
     * @param string $format
     * @return string
     */
    public function getFormattedValue(string $format): string
    {
        return sprintf($format, ...array_values($this->colorInformation));
    }

    /**
     * Returns color values in RGBA
     *
     * @return RGBA
     * @throws InvalidInputNumberException
     */
    public function getRGBA(): RGBA
    {
        $CMYK = $this->getCMYK();
        $RGB = $this->converter->getRGBFromCMYK($CMYK);
        
        return new RGBA(
            $RGB->getValue('R'),
            $RGB->getValue('G'),
            $RGB->getValue('B'),
            1
        );
    }

    /**
     * Returns all color values
     *
     * @return array<string, float>
     */
    public function getValues(): array
    {
        return $this->colorInformation;
    }

    /**
     * Returns color values in HSL
     *
     * @return HSL
     * @throws InvalidInputNumberException
     */
    public function getHSL(): HSL
    {
        $RGB = $this->getRGB();
        return $this->converter->getHSLFromRGB($RGB);
    }

    /**
     * Returns color values in HSLA
     *
     * @return HSLA
     * @throws InvalidInputNumberException
     */
    public function getHSLA(): HSLA
    {
        $HSL = $this->getHSL();
        return $this->converter->getHSLAFromHSL($HSL);
    }

    /**
     * Returns the complementary color
     *
     * @return Complementary
     * @throws InvalidInputNumberException
     */
    public function getComplementary(): Complementary
    {
        return new Complementary($this->getHSL());
    }
}
