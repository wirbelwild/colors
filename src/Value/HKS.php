<?php

/**
 * Colors.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @license MIT
 */

namespace Color\Value;

use Color\System\HKS as HKSSystem;
use Color\System\SystemInterface;
use Color\Value\Exception\InvalidInputNumberException;
use Color\Value\Exception\InvalidValue;

/**
 * Holds HKS values
 * @see \Color\Tests\Value\HKSTest
 */
class HKS implements ValueInterface
{
    /**
     * Instance of converter
     *
     * @var Converter
     */
    private $converter;
    
    /**
     * @var string
     */
    private $name;
    
    /**
     * @var \Color\System\HKS
     */
    private $colorSystem;
    
    /**
     * @var array<string, mixed>
     */
    private $colorInformation;

    /**
     * Set up a color
     *
     * @param string $colorName The name of the color
     * @param \Color\System\HKS $colorSystem The name of the color system
     * @throws InvalidValue
     */
    public function __construct(string $colorName, HKSSystem $colorSystem)
    {
        $colorInformation = $colorSystem->getColorInformation($colorName);
        $this->name = $colorName;
        $this->colorSystem = $colorSystem;
        $this->colorInformation = $colorInformation;
        $this->converter = new Converter();
    }

    /**
     * @return array<string, mixed>
     */
    public function __serialize(): array
    {
        return [
            'cI' => $this->colorInformation,
            'cSC' => $this->colorSystem,
        ];
    }

    /**
     * @param array<string, mixed> $data
     */
    public function __unserialize(array $data): void
    {
        $this->colorInformation = $data['cI'];
        $this->colorSystem = $data['cSC'];
        $this->converter = new Converter();
    }

    /**
     * Returns the color system
     *
     * @return SystemInterface
     */
    public function getSystem(): SystemInterface
    {
        return $this->colorSystem;
    }
    
    /**
     * Returns a single color value
     *
     * @param string $value
     * @return mixed
     */
    public function getValue(string $value)
    {
        return $this->colorInformation[$value];
    }
    
    /**
     * Returns color values in CMYK
     *
     * @return CMYK
     */
    public function getCMYK(): CMYK
    {
        $colorValues = $this->getValue('values');

        if (array_key_exists('CMYK', $colorValues)) {
            return $colorValues['CMYK'];
        }

        return array_values($colorValues)[0]->getCMYK();
    }
    
    /**
     * Returns color values in CIELab
     *
     * @return CIELab
     */
    public function getCIELab(): CIELab
    {
        $colorValues = $this->getValue('values');

        if (array_key_exists('CIELab', $colorValues)) {
            return $colorValues['CIELab'];
        }

        return array_values($colorValues)[0]->getCIELab();
    }

    /**
     * Returns color values in RGB
     *
     * @return RGB
     */
    public function getRGB(): RGB
    {
        $colorValues = $this->getValue('values');

        if (array_key_exists('RGB', $colorValues)) {
            return $colorValues['RGB'];
        }

        return array_values($colorValues)[0]->getRGB();
    }

    /**
     * Returns color values in RGBA
     *
     * @return RGBA
     * @throws InvalidInputNumberException
     */
    public function getRGBA(): RGBA
    {
        $RGB = $this->getRGB();

        return new RGBA(
            $RGB->getValue('R'),
            $RGB->getValue('G'),
            $RGB->getValue('B'),
            1
        );
    }
    
    /**
     * Returns color values in HEX
     *
     * @return HEX
     */
    public function getHEX(): HEX
    {
        $colorValues = $this->getValue('values');

        if (array_key_exists('HEX', $colorValues)) {
            return $colorValues['HEX'];
        }

        return array_values($colorValues)[0]->getHEX();
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return $this->getName();
    }

    /**
     * Returns the color value as a formatted string
     *
     * @param string $format
     * @return string
     */
    public function getFormattedValue(string $format): string
    {
        return sprintf($format, $this->getName());
    }

    /**
     * Returns all color values
     *
     * @return array<int, string>
     */
    public function getValues(): array
    {
        return [$this->name];
    }

    /**
     * Returns color values in HSL
     *
     * @return HSL
     * @throws InvalidInputNumberException
     */
    public function getHSL(): HSL
    {
        $RGB = $this->getRGB();
        return $this->converter->getHSLFromRGB($RGB);
    }

    /**
     * Returns color values in HSLA
     *
     * @return HSLA
     * @throws InvalidInputNumberException
     */
    public function getHSLA(): HSLA
    {
        $HSL = $this->getHSL();
        return $this->converter->getHSLAFromHSL($HSL);
    }

    /**
     * Returns the complementary color
     *
     * @return Complementary
     * @throws InvalidInputNumberException
     */
    public function getComplementary(): Complementary
    {
        return new Complementary($this->getHSL());
    }
}
