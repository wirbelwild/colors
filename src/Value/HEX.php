<?php

/**
 * Colors.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @license MIT
 */

namespace Color\Value;

use Color\System\HEX as HEXSystem;
use Color\System\SystemInterface;
use Color\Value\Exception\InvalidInputLengthRangeException;
use Color\Value\Exception\InvalidInputNumberException;

/**
 * Holds HEX values
 * @see \Color\Tests\Value\HEXTest
 */
class HEX implements ValueInterface
{
    /**
     * Instance of converter
     *
     * @var Converter
     */
    private $converter;
    
    /**
     * Color values
     *
     * @var array<string, string>
     */
    private $colorInformation;
    
    /**
     * @var \Color\System\HEX
     */
    private $colorSystem;
    
    /**
     * @var string
     */
    private $name;

    /**
     * Sets up a new HEX color.
     *
     * @param string                 $HEX         The HEX value. This may be a color with 3 characters like `#fc0`
     *                                            or a color with 6 characters like `#ffcc00`. It is also possible
     *                                            to add the alpha channel, in this case a 8 character string is needed,
     *                                            for example `#ffcc00aa`. It doesn't matter whether the # is set or not.
     * @param \Color\System\HEX|null $colorSystem The color system.
     * @throws InvalidInputLengthRangeException
     */
    public function __construct(string $HEX, ?HEXSystem $colorSystem = null)
    {
        $HEX = ltrim($HEX, '#');
        
        ValueValidation::validateLengthOptional($HEX, 3, 6, 8);

        $this->colorInformation = [
            'HEX' => $HEX,
        ];
        $this->colorSystem = $colorSystem ?? new HEXSystem();
        $this->converter = new Converter();
        $this->name = $HEX;
    }


    /**
     * @return array<string, mixed>
     */
    public function __serialize(): array
    {
        return [
            'cI' => $this->colorInformation,
            'cSC' => $this->colorSystem,
        ];
    }

    /**
     * @param array<string, mixed> $data
     */
    public function __unserialize(array $data): void
    {
        $this->colorInformation = $data['cI'];
        $this->colorSystem = $data['cSC'];
        $this->converter = new Converter();
    }

    /**
     * Returns the color system
     *
     * @return SystemInterface
     */
    public function getSystem(): SystemInterface
    {
        return $this->colorSystem;
    }
    
    /**
     * Returns a single color value
     *
     * @param string $value
     * @return string
     */
    public function getValue(string $value): string
    {
        return $this->colorInformation[$value];
    }

    /**
     * Returns color values in RGB
     *
     * @return RGB
     * @throws InvalidInputNumberException
     */
    public function getRGB(): RGB
    {
        $HEX = $this->getHEX();
        $RGBA = $this->converter->getRGBAFromHEX($HEX);
        return $this->converter->getRGBFromRGBA($RGBA);
    }

    /**
     * Returns color values in RGBA
     *
     * @return RGBA
     * @throws InvalidInputNumberException
     */
    public function getRGBA(): RGBA
    {
        $HEX = $this->getHEX();
        return $this->converter->getRGBAFromHEX($HEX);
    }

    /**
     * Returns color values in CMYK
     *
     * @return CMYK
     * @throws InvalidInputNumberException
     */
    public function getCMYK(): CMYK
    {
        $HEX = $this->getHEX();
        $RGB = $this->converter->getRGBFromHEX($HEX);
        $CMY = $this->converter->getCMYFromRGB($RGB);
        return $this->converter->getCMYKFromCMY($CMY);
    }
    
    /**
     * Returns color values in HEX
     *
     * @return HEX
     */
    public function getHEX(): HEX
    {
        return $this;
    }

    /**
     * Returns color values in CIELab
     *
     * @return CIELab
     * @throws InvalidInputNumberException
     */
    public function getCIELab(): CIELab
    {
        $HEX = $this->getHEX();
        $RGB = $this->converter->getRGBFromHEX($HEX);
        $XYZ = $this->converter->getXYZFromRGB($RGB);
        return $this->converter->getCIELabFromXYZ($XYZ);
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return '#' . implode('', $this->colorInformation);
    }

    /**
     * Returns the color value as a formatted string
     *
     * @param string $format
     * @return string
     */
    public function getFormattedValue(string $format): string
    {
        return sprintf($format, ...array_values($this->colorInformation));
    }

    /**
     * Returns all color values
     *
     * @return array<string, string>
     */
    public function getValues(): array
    {
        return $this->colorInformation;
    }

    /**
     * Returns color values in HSL
     *
     * @return HSL
     * @throws InvalidInputNumberException
     */
    public function getHSL(): HSL
    {
        $RGB = $this->getRGB();
        return $this->converter->getHSLFromRGB($RGB);
    }

    /**
     * Returns color values in HSLA
     *
     * @return HSLA
     * @throws InvalidInputNumberException
     */
    public function getHSLA(): HSLA
    {
        $HSL = $this->getHSL();
        return $this->converter->getHSLAFromHSL($HSL);
    }

    /**
     * Returns the complementary color
     *
     * @return Complementary
     * @throws InvalidInputNumberException
     */
    public function getComplementary(): Complementary
    {
        return new Complementary($this->getHSL());
    }
}
