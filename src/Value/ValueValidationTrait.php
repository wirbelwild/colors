<?php

/**
 * Colors.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @license MIT
 */

namespace Color\Value;

use Color\Value\Exception\InvalidInputLengthException;
use Color\Value\Exception\InvalidInputNumberException;

/**
 * Trait ValueValidationTrait
 *
 * @package Color\Value
 * @deprecated This trait has been deprecated.
 * @todo Remove in v3.0.
 */
trait ValueValidationTrait
{
    /**
     * @param int|float $input
     * @param int $min
     * @param int $max
     * @return void
     * @throws InvalidInputNumberException
     * @deprecated The method `validateNumber` has been deprecated. Use `ValueValidation::validateNumberRange()` instead.
     * @see \Color\Value\ValueValidation::validateNumberRange()
     * @todo Remove in v3.0.
     */
    public function validateNumber($input, int $min, int $max): void
    {
        trigger_error('The method `validateNumber` has been deprecated. Use `ValueValidation::validateNumberRange()` instead.', E_USER_DEPRECATED);
        ValueValidation::validateNumberRange($input, $min, $max);
    }

    /**
     * @param string|int|float $input
     * @param int $min
     * @param int $max
     * @return void
     * @throws InvalidInputLengthException
     * @deprecated The method `validateLength` has been deprecated. Use `ValueValidation::validateLengthRange()` instead.
     * @see \Color\Value\ValueValidation::validateLengthRange()
     * @todo Remove in v3.0.
     */
    public function validateLength($input, int $min, int $max): void
    {
        trigger_error('The method `validateLength` has been deprecated. Use `ValueValidation::validateLengthRange()` instead.', E_USER_DEPRECATED);
        ValueValidation::validateLengthRange($input, $min, $max);
    }
}
