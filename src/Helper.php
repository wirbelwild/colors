<?php

/**
 * Colors.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @license MIT
 */

namespace Color;

/**
 * Class Helper.
 *
 * @package Color
 * @see \Color\Tests\HelperTest
 */
class Helper
{
    /**
     * @param float $percent
     * @return string
     */
    public static function getHexFromPercent(float $percent): string
    {
        $intValue = (int) round($percent / 100 * 255);
        $hexValue = dechex($intValue);
        return str_pad($hexValue, 2, '0');
    }

    /**
     * @param string $hex
     * @param int $round
     * @return float
     */
    public static function getPercentFromHex(string $hex, int $round = 0): float
    {
        $intValue = hexdec($hex);
        return round(($intValue / 255) * 100, $round);
    }
}
