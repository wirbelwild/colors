# Changes in Bit&Black Colors 2.10

## 2.10.0 2021-04-01

### Added

-   Hex colors may contain the 8-bit alpha transparency value now. A hex color may look like `#fc0`, `#ffcc00` or `#ffcc00aa`.

### Changed

-   The `ValueValidationTrait` has been deprecated and replaced by the `ValueValidation` class.